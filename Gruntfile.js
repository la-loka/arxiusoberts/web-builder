/*
 *
 * Gruntfile moare presentations Bootstrap
 *
 */

'use strict';
module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    //Bower
    bower:{
      install: {
        options: {
          targetDir: 'app/assets/js',
          layoud: 'byComponent',
          install: true,
          cleanBowerDir: true
        }
      }
    },

    //Compass
    compass: {
      dev: {
        options: {
          httpPath: '/',
          cssDir: 'app/assets/stylesheets',
          sourcemap: true,
          sassDir: 'app/assets/scss',
          imagesDir: 'app/assets/images',
          javascriptsDir: 'app/assets/js',
          fontsDir: 'app/assets/fonts',
          outputStyle: 'expanded',
          environment: 'development'
        }
      },
      prod: {
        options: {
          httpPath: '/',
          sassDir:'app/assets/scss',
          specify: 'app/assets/scss/main.scss',
          cssDir:'dist/assets/stylesheets',
          sourcemap: true,
          outputStyle: 'compressed',
          environment: 'production'
        }
      }
    },

    // Concat
    concat: {
      options: {
        separator: ';',
      },
      dist: {
        src: [
        // By componenets
        'assets/js/lib/boostrap-sass/bootstrap/affix.js',
        'assets/js/lib/boostrap-sass/bootstrap/alert.js',
        'assets/js/lib/boostrap-sass/bootstrap/button.js',
        'assets/js/lib/boostrap-sass/bootstrap/carousel.js',
        'assets/js/lib/boostrap-sass/bootstrap/collapse.js',
        'assets/js/lib/boostrap-sass/bootstrap/dropdown.js',
        'assets/js/lib/boostrap-sass/bootstrap/modal.js',
        'assets/js/lib/boostrap-sass/bootstrap/popover.js',
        'assets/js/lib/boostrap-sass/bootstrap/scrollspy.js',
        'assets/js/lib/boostrap-sass/bootstrap/tab.js',
        'assets/js/lib/boostrap-sass/bootstrap/tooltip.js',
        'assets/js/lib/boostrap-sass/bootstrap/transition.js'
        ],
        // Concatinate my Foundation
        dest: 'assets/js/lib/boostrap-sass/my-bootstrap.js',
      },
    },

    //Uglify
    uglify: {
      options: {
        preserveComments: false,
      },
      all: {
        files: {
          'dist/assets/js/lib/jquery.min.js': ['app/assets/js/lib/jquery/jquery.js'],
          'dist/assets/js/lib/bootstrap.min.js': ['app/assets/js/lib/bootstrap-sass/bootstrap.js']
        }
      }
    },

    //Watch
    watch: {
      css: {
        files: ['app/assets/scss/*.scss','app/assets/scss/specific/*.scss','app/assets/scss/plugins/*.scss'],
        tasks: ['compass:dev']
      }
    },
    
    //Copy
    copy: {
      main: {
        files: [{
          expand: true,
          cwd: 'app/',
          src: ['**/*.html'],
          dest: 'dist/'
        }]
      },
      images: {
        files: [{
          expand: true,
          cwd: 'app/assets/imgs',
          src: ['*.{png,jpg,svg,gif}'],
          dest: 'dist/assets/imgs'
        }]
      },
      imagescss: {
        files: [{
          expand: true,
          cwd: 'app/assets/imgs/css',
          src: ['*.{png,jpg,svg,gif}'],
          dest: 'dist/assets/imgs/css'
        }]
      },
      logos: {
        files: [{
          expand: true,
          cwd: 'app/',
          src: ['logo.{png,jpg,svg,gif}', 'favicon.ico'],
          dest: 'dist/'
        }]
      },
      robots: {
        files: [{
          expand: true,
          cwd: 'app/',
          src: ['robots.txt'],
          dest: 'dist/'
        }]
      },
      fonts:{
        files: [{
          expand: true,
          cwd: 'app/assets/fonts',
          src: ['**/*.*'],
          dest: 'dist/assets/fonts'
        }]
      },
    },

    //Replace
    'string-replace': {
      inline: {
        files: [{
          expand: true,
          cwd: 'dist/',
          src: '**/*.html',
          dest: 'dist/',
        }],
        options: {
          replacements: [
            {
              pattern: '<link rel="stylesheet" href="/assets/stylesheets/main.css">',
              replacement: '<link rel="stylesheet" href="/assets/stylesheets/main.css">'
            },
            {
              pattern: '<script src="/assets/js/lib/jquery/jquery.js"></script>',
              replacement: '<script src="/assets/js/lib/jquery.min.js"></script>'
            },
            {
              pattern: '<script src="/assets/js/lib/bootstrap-sass/bootstrap.js"></script>',
              replacement: '<script src="/assets/js/lib/bootstrap.min.js"></script>'
            }
          ]
        }
      }
    }

  });

  // Load tasks
  grunt.loadNpmTasks('grunt-bower-task');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-string-replace');

  // Register tasks
  grunt.registerTask('default', ['compass:dev','watch']);
  grunt.registerTask('prod', ['compass:prod','copy','uglify','string-replace']);
};
